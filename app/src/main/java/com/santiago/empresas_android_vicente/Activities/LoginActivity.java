package com.santiago.empresas_android_vicente.Activities;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.santiago.empresas_android_vicente.Models.LoginResponse;
import com.santiago.empresas_android_vicente.R;
import com.santiago.empresas_android_vicente.Services.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText txtEmail;
    private EditText txtSenha;
    private TextView credenciais;
    private Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtSenha = (EditText) findViewById(R.id.txtSenha);
        credenciais = (TextView) findViewById(R.id.txtCredenciais);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                entrar();
            }
        });
    }

    private void entrar() {
        String email = txtEmail.getText().toString().trim();
        String senha = txtSenha.getText().toString().trim();

        if(email.isEmpty()){
            txtEmail.setError("E-mail obrigatório");
            txtEmail.requestFocus();
            return;
        }

        if(senha.isEmpty()){
            txtSenha.setError("Senha obrigatória");
            txtSenha.requestFocus();
            return;
        }

        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(email, senha);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                LoginResponse loginResponse = response.body();

                if (loginResponse!= null) {

//                    Toast.makeText(LoginActivity.this, "login", Toast.LENGTH_LONG).show();
                    Intent it = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(it);
                    credenciais.setVisibility(View.GONE);

                } else {
                    credenciais.setVisibility(View.VISIBLE);
//                    Toast.makeText(LoginActivity.this, "Usuário ou senha inválidos", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
    }
}