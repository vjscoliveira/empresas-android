package com.santiago.empresas_android_vicente.Models;

public class LoginResponse {

    private Investor investor;
    private String enterprise;
    private boolean success;

    public LoginResponse(Investor investor, String enterprise, boolean success) {
        this.investor = investor;
        this.enterprise = enterprise;
        this.success = success;
    }

    public Investor getInvestor() {
        return investor;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public boolean isSuccess() {
        return success;
    }
}
