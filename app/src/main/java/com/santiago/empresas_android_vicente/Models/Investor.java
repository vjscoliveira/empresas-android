package com.santiago.empresas_android_vicente.Models;

import java.util.List;

public class Investor {

    private int id;
    private String investor_name;
    private String email;
    private String city;
    private String country;
    private double balance;
    private String photo;
    private List portifolio;
    private double portfolio_value;
    private boolean first_access;
    private boolean super_angel;

    Investor(int id, String investor_name, String email, String city, String country,
             double balance, String photo, List portifolio, double portfolio_value,
             boolean first_access, boolean super_angel) {
        this.id = id;
        this.investor_name = investor_name;
        this.email = email;
        this.city = city;
        this.country = country;
        this.balance = balance;
        this.photo = photo;
        this.portifolio = portifolio;
        this.portfolio_value = portfolio_value;
        this.first_access = first_access;
        this.super_angel = super_angel;
    }

    public int getId() {
        return id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public double getBalance() {
        return balance;
    }

    public String getPhoto() {
        return photo;
    }

    public List getPortifolio() {
        return portifolio;
    }

    public double getPortfolio_value() {
        return portfolio_value;
    }

    public boolean isFirst_access() {
        return first_access;
    }

    public boolean isSuper_angel() {
        return super_angel;
    }
}
