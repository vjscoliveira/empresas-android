package com.santiago.empresas_android_vicente.Models;

import com.google.gson.annotations.SerializedName;


public class Enterprises {

    int id;
    String enterprise_name;
    String photo;
    String description;
    String country;
    String enterprise_type;

    public Enterprises(int id, String enterprise_name, String photo, String description, String country, String enterprise_type) {
        this.id = id;
        this.enterprise_name = enterprise_name;
        this.photo = photo;
        this.description = description;
        this.country = country;
        this.enterprise_type = enterprise_type;
    }

    public int getId() {
        return id;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public String getPhoto() {
        return photo;
    }

    public String getDescription() {
        return description;
    }

    public String getCountry() {
        return country;
    }

    public String getEnterprise_type() {
        return enterprise_type;
    }

}
