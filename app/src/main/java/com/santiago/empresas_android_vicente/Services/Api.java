package com.santiago.empresas_android_vicente.Services;

import com.santiago.empresas_android_vicente.Models.Enterprises;
import com.santiago.empresas_android_vicente.Models.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

    @FormUrlEncoded
    @POST("users/auth/sign_in")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @Headers({
            "access-token: zqITNpnhiFGdVw86ItBo2Q",
            "client: hxHruHqJpirUs7PQABDcLw",
            "uid: testeapple@ioasys.com.br"
    })
//    @GET("dds861/3932d2ad026a64ccea9c86e5d20ac9b5/raw/26c68320633242159631f263a35def193bc7c3a8/json.json")
    @GET("enterprises?enterprise_types=3")
    Call<JSONResponse> getJSON();
}
