package com.santiago.empresas_android_vicente.Adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.santiago.empresas_android_vicente.Models.Enterprises;
import com.santiago.empresas_android_vicente.R;

import java.util.ArrayList;
import java.util.List;

public class EnterpriseAdapter extends RecyclerView.Adapter<EnterpriseAdapter.ViewHolder> {

    private ArrayList<Enterprises> enterprises;

    public EnterpriseAdapter(ArrayList<Enterprises> enterprises) {
        this.enterprises = enterprises;
    }

    @Override
    public EnterpriseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EnterpriseAdapter.ViewHolder holder, int position) {
        holder.tv_name.setText(enterprises.get(position).getDescription());
        holder.tv_version.setText(enterprises.get(position).getEnterprise_name());
        holder.tv_api_level.setText(enterprises.get(position).getCountry());
    }

    @Override
    public int getItemCount() {
        return enterprises.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private TextView tv_version;
        private TextView tv_api_level;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_name = (TextView)itemView.findViewById(R.id.tv_name);
            tv_version = (TextView)itemView.findViewById(R.id.tv_version);
            tv_api_level = (TextView)itemView.findViewById(R.id.tv_api_level);

        }
    }
}